package sample

import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.ext.web.Router

class Server : AbstractVerticle() {

    override fun start() {
        val router = Router.router(vertx)
        router.get("/").handler { ctx -> ctx.response().end("<h1>hello, world</h1>") }
        vertx.createHttpServer().requestHandler(router::accept).listen(8080)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val vertx = Vertx.vertx()
            vertx.deployVerticle(Server())
        }
    }

}
